const GoogleSpreadsheet = require('google-spreadsheet');
const { promisify } = require('util');

const creds = require('./client_secret.json');

// DATABASE //
let member = ['Lukas','Pearl','Annika','Pedro','Malvin',
              'Talitha','Emma','Anna','Andres','Placeholder',
              'Kasimir','Lea','Clay','Marius','Lena','Elly'];;

let availableMembers = ['Lukas','Pearl','Annika','Pedro','Malvin',
                       'Talitha','Emma','Anna','Andres','Placeholder',
                       'Kasimir','Lea','Clay','Marius','Lena','Elly'];

function dontIncludeLukas(name) {
    return name !== "Lukas";
}

function dontIncludePearl(name) {
    return name !== "Pearl";
}

function dontIncludeAnnika(name) {
    return name !== "Annika";
}

function dontIncludePedro(name) {
    return name !== "Pedro";
}

function dontIncludeMalvin(name) {
    return name !== "Malvin";
}

function dontIncludeTalitha(name) {
    return name != "Talitha";
}

function dontIncludeEmma(name) {
    return name !== "Emma";
}

function dontIncludeAnna(name) {
    return name !== "Anna";
}

function dontIncludeAndres(name) {
    return name !== "Andres";
}

function dontIncludePlaceholder(name) {
    return name !== "Placeholder";
}

function dontIncludeKasimir(name) {
    return name !== "Kasimir";
}

function dontIncludeLea(name) {
    return name !== "Lea";
}

function dontIncludeClay(name) {
    return name !== "Clay";
}

function dontIncludeMarius(name) {
    return name !== "Marius";
}

function dontIncludeLena(name) {
    return name !== "Lena";
}

function dontIncludeElly(name) {
    return name !== "Elly";
}

function dontInclude(forbiddenNames) {
    for(let i=0;i<forbiddenNames.length;i++) {
        switch (forbiddenNames[i]) {
            case 'Lukas':
                availableMembers = availableMembers.filter(dontIncludeLukas);
                break;
            case 'Pearl':
                availableMembers = availableMembers.filter(dontIncludePearl);
                break;
            case 'Annika':
                availableMembers = availableMembers.filter(dontIncludeAnnika);
                break;
            case 'Pedro':
                availableMembers = availableMembers.filter(dontIncludePedro);
                break;
            case 'Malvin':
                availableMembers = availableMembers.filter(dontIncludeMalvin);
                break;
            case 'Talitha':
                availableMembers = availableMembers.filter(dontIncludeTalitha);
                break;
            case 'Emma':
                availableMembers = availableMembers.filter(dontIncludeEmma);
                break;
            case 'Anna':
                availableMembers = availableMembers.filter(dontIncludeAnna);
                break;
            case 'Andres':
                availableMembers = availableMembers.filter(dontIncludeAndres);
                break;
            case 'Placeholder':
                availableMembers = availableMembers.filter(dontIncludePlaceholder);
                break;
            case 'Kasimir':
                availableMembers = availableMembers.filter(dontIncludeKasimir);
                break;
            case 'Lea':
                availableMembers = availableMembers.filter(dontIncludeLea);
                break;
            case 'Clay':
                availableMembers = availableMembers.filter(dontIncludeClay)
                break;
            case 'Marius':
                availableMembers = availableMembers.filter(dontIncludeMarius);
                break;
            case 'Lena':
                availableMembers = availableMembers.filter(dontIncludeLena);
                break;
            case 'Elly':
                availableMembers = availableMembers.filter(dontIncludeElly);
                break;
            default:
                break;
        }
    }
}

// Functionality to Google Spreadsheet
function updateDailyAssignment(data,member) {
    let j = 0;
    for (let i = 0; i < data.length; i++) {
        if (i % 2 === 0) {
            data[i].türklinkenundkühlschrankgriffe = member[(j%availableMembers.length)];
            data[i].oberflächenwishenunddesinfizieren = member[((j+1)%availableMembers.length)];
            data[i].spüleputzen = member[((j+2)%availableMembers.length)];
            data[i].handtuchindieküchehängen = member[((j+3)%availableMembers.length)];
            j = (j + 4)%availableMembers.length;
            data[i].save();
        }
    }
}

function updateWeeklyAssignment(data,member) {
    data[0].bodenwischen = member[0];
    data[0].schwammaustauschen = member[1];
    data[0].save();

}

function createWeeklyMember(lastWeeklyMember) {
    //FIXME: what if the lastWeeklyMember is not available for the coming week?
    let indexLastWeeklyMember = availableMembers.indexOf(lastWeeklyMember);

    let weeklyMembers = new Array(2);
    weeklyMembers[0] = availableMembers[(indexLastWeeklyMember+1)%availableMembers.length];
    weeklyMembers[1] = availableMembers[(indexLastWeeklyMember+2)%availableMembers.length];
    
    return weeklyMembers;
}

async function accessSpreadsheet() {
    const doc = new GoogleSpreadsheet('1eDwyINM-sVHdSrNRPK4lJPqKRhyP-M06QtrPcwdX4WQ')
    await promisify(doc.useServiceAccountAuth)(creds);
    const info = await promisify(doc.getInfo)();
    const sheet = info.worksheets[0];

    //Get the data from Google Spreadsheet
    const data = await promisify(sheet.getRows)({
        offset: 1
    });
    
    // TODO: await updateDates(sheet,data);
    await updateAssignments(sheet,data);

}

async function updateAssignments(sheet,data) {
    //Get database
    const forbiddenNames = ['Placeholder', 'Anna', 'Annika'];
    console.log("Forbidden Names: ");
    console.log(forbiddenNames);

    dontInclude(forbiddenNames);

    console.log("\nAvailable Members: ");
    console.log(availableMembers);

    //Create array for weekly assignment
    const lastWeeklyMember = data[0].bodenwischen;
    const weeklyMembers = createWeeklyMember(lastWeeklyMember);
    console.log("\nWeekly Members: ");
    console.log(weeklyMembers);
    
    //Create array for daily assignment
    dontInclude(weeklyMembers);
    console.log("\nDaily Members: ");
    console.log(availableMembers);

    const lastDailyMember = data[12].handtuchindieküchehängen;
    let dailyMembers = new Array(availableMembers.length);

    //FIXME: what if the lastDailyMember is not available for coming week?
    let indexNewDailyMember = availableMembers.indexOf(lastDailyMember) + 1;
    for (let k = 0; k < availableMembers.length; k++) {
        dailyMembers[k] = availableMembers[indexNewDailyMember];
        indexNewDailyMember = (indexNewDailyMember + 1) % availableMembers.length;
    }

    console.log("\nDaily Members: ");
    console.log(dailyMembers);

    updateWeeklyAssignment(data,weeklyMembers);
    updateDailyAssignment(data,dailyMembers);
}

//DEBUGGING TOOLS
function printRow(rows) {
    rows.forEach(row => {
        console.log(row.datum + " " + row.türklinkenundkühlschrankgriffe + " "
            + row.oberflächenwishenunddesinfizieren + " " + row.spüleputzen +
            " " + row.handtuchindieküchehängen);
    });
}

accessSpreadsheet();